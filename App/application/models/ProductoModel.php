<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class ProductoModel extends CI_Model {

        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        function getproducto($keyword = null)
        {
            $this->db->select('ID,titulo,descripcion, fecha_inicio, fecha_termino');
        	if($keyword != null){
        		$this->db->like('UPPER(titulo)',strtoupper($keyword));
        	}
            $this->db->limit(50);
            $this->db->order_by('titulo', 'ASC');
            $query = $this->db->get('productos');
            //echo $this->db->last_query();
            return $query->result_array();
        }

        function estadistica($id, $keyword){
            $data = array('producto_id' => $id, 'keyword' => $keyword);
            $this->db->where($data);
            $query = $this->db->get('estadistica')->result_array();
            if(!empty($query)){
                //echo "existe";
                $this->db->where($data);
                $this->db->update('estadistica',array('count' => $query[0]['count']+1));
            }else{
                //echo "no existe";
                $this->db->insert('estadistica', array('producto_id' => $id, 'keyword' => $keyword, 'count' => 1));
            }
        }
    }
?>