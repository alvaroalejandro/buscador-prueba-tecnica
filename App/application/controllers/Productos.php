<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
	public function search($keyword = null)
	{
		$this->load->model('ProductoModel');
		if($keyword == null){
			$keyword = $this->input->post('keyword');
		}
		$data = $this->ProductoModel->getproducto($keyword);
		if($keyword != null){
			foreach ($data as $key => $producto) {
			$this->ProductoModel->estadistica($producto['ID'],$keyword);			
			}
		}
		echo json_encode($data);
	}
}
