<?php $this->load->view('header'); ?> 
<body class="container"> <h1>ESTADISTICAS</h1>
  <div class="col-sm-10">
	<div>
    <br>
  <table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">TITULO</th>
      <th scope="col">BUSQUEDAS</th>
      <th scope="col">PALABRAS CLAVES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($estadisticas as $key => $estadistica) {?>
        <tr>
          <td><?php echo $estadistica['titulo'] ?></td>
          <td><?php echo $estadistica['total'] ?></td>
          <td>
            <?php 
            $keywords = "";
            foreach ($estadistica['keyword'] as $key => $keyword) {
                $keywords.= $keyword['keyword']. " ,";
            } 
            echo $keywords?>
          </td>
        </tr>

    <?php } ?>
  </tbody>

</table>
</body> 
