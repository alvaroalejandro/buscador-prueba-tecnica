 <?php $this->load->view('header'); ?> 
<body class="container"> <h1>BUSCADOR</h1>
  <div class="col-sm-10">
	<div>
    <br>
  <table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">TITULO</th>
      <th scope="col">DESCRIPCION</th>
      <th scope="col">FECHA INICIO</th>
      <th scope="col">FECHA TERMINO</th>
    </tr>
  </thead>
  <tbody></tbody>

</table>
</body> 
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Titulo</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Escriba aquí">
    </div>
  </div>
  <button type="submit" class="btn btn-primary" onclick="buscar();">Buscar</button>
<script>
	function buscar()
	{
		var keyword = $("#keyword").val();
		if(keyword.length>0){
			$.ajax({
		        data: {'keyword' : keyword},
		        type: "POST",
		        url: 'http://localhost/app/index.php/productos/search',
		        dataType: 'Json',
		        success : function(data) {
		          	var html = "";
		          	$(".table tbody").html('');
		          	$.each( data, function( key, value ) {
			          	key = key+1;
					  	html += '<tr>';
					  	html += '<td>'+value.ID+'</td>';
					  	html += '<td>'+value.titulo+'</td>';
					 	html += '<td>'+value.descripcion+'</td>';
					  	html += '<td>'+value.fecha_inicio+'</td>';
					  	html += '<td>'+value.fecha_termino+'</td>';
					  	html += '</tr>';
					});
		          	$(".table tbody").append(html);
		          	$('#keyword').val('');
		        }
	      	});
		}else{
			alert("Debe agregar un titulo para buscar")
		}
	}
</script>
