<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadistica extends CI_Controller {
	
	public function index()
	{
		$this->load->model('EstadisticaModel');
		$data['estadisticas'] = $this->EstadisticaModel->getestadistica();
		$this->load->view('estadistica',$data);
	}
}
