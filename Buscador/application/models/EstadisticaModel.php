<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class EstadisticaModel extends CI_Model {

        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        function getestadistica()
        {
            $this->db->select('producto_id, p.titulo,SUM(count) AS total');
            $this->db->join('productos p', 'p.ID = producto_id');
            $this->db->group_by('producto_id');
            $this->db->order_by('total', 'DESC');
            $this->db->limit(20);
            $query = $this->db->get('estadistica')->result_array();
            //echo $this->db->last_query();
            foreach ($query as $key => $q) {
                $this->db->where('producto_id', $q['producto_id']);
                $this->db->order_by('count','DESC');
                $this->db->limit(5);
                $query[$key]['keyword'] = $this->db->get('estadistica')->result_array();
            }
            return $query;
        }
    }
?>